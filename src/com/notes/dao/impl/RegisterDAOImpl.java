package com.notes.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.notes.dao.api.RegisterDAO;
import com.notes.utility.DataHandler;

public class RegisterDAOImpl implements RegisterDAO {
    DataHandler dataHandler = new DataHandler();
    static Logger logger = Logger.getLogger(RegisterDAOImpl.class);

    public String isUserExists(String userName) {
	Session session = null;
	String message = "USER_NOT_PRESENT";
	try {
	    session = dataHandler.getSession();
	    Query query = session.getNamedQuery("user.hbm.xml.isUserExists");
	    query.setParameter("userName", userName);
	    List userlist = query.list();
	    if (userlist.size() > 0) {
		message = "USER_PRESENT";
	    }
	} catch (Exception e) {
	    logger.error("Error in getting data", e);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return message;
    }

}
