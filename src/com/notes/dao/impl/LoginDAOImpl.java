package com.notes.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.notes.dao.api.LoginDAO;
import com.notes.utility.DataHandler;

public class LoginDAOImpl implements LoginDAO {

    DataHandler dataHandler = new DataHandler();
    static Logger logger = Logger.getLogger(LoginDAOImpl.class);

    public List<?> getUserDetails(String userName) {
	Session session = null;
	List<?> userList = null;
	try {
	    session = dataHandler.getSession();
	    Query query = session.getNamedQuery("user.hbm.xml.getUserDetails");
	    query.setParameter("userName", userName);
	    userList = query.list();
	} catch (Exception e) {
	    logger.error("Error in getting user details", e);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return userList;
    }
}
