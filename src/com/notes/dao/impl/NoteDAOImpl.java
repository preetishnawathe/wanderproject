package com.notes.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.notes.dao.api.NoteDAO;
import com.notes.model.Note;
import com.notes.utility.DataHandler;

public class NoteDAOImpl implements NoteDAO {

    DataHandler dataHandler = new DataHandler();
    static Logger logger = Logger.getLogger(NoteDAOImpl.class);

    public boolean isAddedNote(String noteTitle) {
	Session session = null;
	boolean isNotePresent = false;
	try {
	    session = dataHandler.getSession();
	    Query query = session.getNamedQuery("note.hbm.xml.isNoteExists");
	    query.setParameter("noteTitle", noteTitle);
	    List<?> noteList = query.list();
	    if (noteList.size() > 0) {
		isNotePresent = true;
	    }
	} catch (Exception e) {
	    logger.error("Error in getting data", e);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return isNotePresent;
    }

    public List<Note> getNoteDetails(Long userId) {
	Session session = null;
	List<Note> noteList = new ArrayList<Note>();
	try {
	    session = dataHandler.getSession();
	    Query query = session.getNamedQuery("note.hbm.xml.getNoteDetails");
	    query.setParameter("noteId", userId);
	    noteList = query.list();
	} catch (Exception e) {
	    logger.error("Error in getting data", e);
	}
	return noteList;
    }

    public boolean deleteNote(Long noteId) {
	Session session = null;
	boolean isSuccessful = true;
	try {
	    session = dataHandler.getSession();
	    Note note = dataHandler.getNoteObject(session, noteId);
	    dataHandler.removeObj(session, note);
	} catch (Exception e) {
	    logger.error("Error in deleting note", e);
	    isSuccessful = false;
	    dataHandler.transactionRollback(session);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return isSuccessful;
    }

    public boolean addNote(Note note) {
	Session session = null;
	boolean isSuccessful = true;
	try {
	    session = dataHandler.getSession();
	    dataHandler.saveObj(session, note);
	} catch (Exception e) {
	    logger.error("Error in adding note", e);
	    isSuccessful = false;
	    dataHandler.transactionRollback(session);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return isSuccessful;
    }

    public Note getNote(Long noteId) {
	Session session = null;
	Note note = null;
	try {
	    session = dataHandler.getSession();
	    note = dataHandler.getNoteObject(session, noteId);
	} catch (Exception e) {
	    logger.error("Error in getting data", e);
	    dataHandler.transactionRollback(session);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return note;
    }

    public List<Note> getNoteByTitle(String noteTitle) {
	Session session = null;
	List<Note> noteList = new ArrayList<Note>();
	try {
	    session = dataHandler.getSession();
	    Query query = session
		    .getNamedQuery("note.hbm.xml.getNoteByNoteTitle");
	    query.setParameter("noteTitle", noteTitle);
	    noteList = query.list();
	} catch (Exception e) {
	    logger.error("Error in getting data", e);
	    dataHandler.transactionRollback(session);
	}
	return noteList;
    }
}
