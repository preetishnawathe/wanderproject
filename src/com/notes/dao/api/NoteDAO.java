package com.notes.dao.api;

import java.util.List;

import com.notes.model.Note;

public interface NoteDAO {

    public boolean isAddedNote(String noteTitle);

    public List<Note> getNoteDetails(Long userId);

    public boolean deleteNote(Long noteId);

    public boolean addNote(Note note);

    public Note getNote(Long noteId);

    public List<Note> getNoteByTitle(String noteTitle);
}
