package com.notes.utility;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Encryption {
    private static final long serialVersionUID = 1L;
    static Logger logger = Logger.getLogger(Encryption.class);

    public Encryption() {
	super();

    }

    public static String encrypt(String str) {
	MessageDigest mdEnc;
	String md5 = null;
	try {
	    mdEnc = MessageDigest.getInstance("MD5");
	    mdEnc.update(str.getBytes(), 0, str.length());
	    md5 = new BigInteger(1, mdEnc.digest()).toString(16);
	} catch (NoSuchAlgorithmException e) {
	    logger.error("Error in encrypting : ", e);
	}
	return md5;

    }
}
