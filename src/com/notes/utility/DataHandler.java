package com.notes.utility;

import org.hibernate.Session;

import com.notes.model.Note;
import com.notes.model.User;

public class DataHandler {

    public Session getSession() {
	HibernateSession hibernateSession = new HibernateSession();
	Session session = hibernateSession.getSession();
	session.beginTransaction();
	return session;
    }

    public void saveObj(Session session, Object dbObj) {
	session.save(dbObj);
	session.getTransaction().commit();
    }

    public void removeObj(Session session, Object dbObj) {
	session.delete(dbObj);
	session.getTransaction().commit();
    }

    public void sessionClose(Session session) {
	HibernateSession hibernateSession = new HibernateSession();
	hibernateSession.sessionClose(session);
    }

    public void transactionRollback(Session session) {
	session.getTransaction().rollback();
    }

    public User getUserObject(Session session, Long userId) {
	return (User) session.get(User.class, userId);
    }

    public void updateObj(Session session, Object dbObj) {
	session.update(dbObj);
	session.getTransaction().commit();
    }

    public void transactionCommit(Session session) {
	session.getTransaction().commit();
    }

    public Note getNoteObject(Session session, Long noteId) {
	return (Note) session.get(Note.class, noteId);

    }

}
