package com.notes.utility;

import java.net.MalformedURLException;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleResources {

    private static ResourceBundle resourceBundle;

    private static void loadResourceBundle(String fileName, Locale locale)
	    throws MalformedURLException {
	resourceBundle = ResourceBundle.getBundle(fileName);

    }

    public static String getValue(String fileName, Locale locale, String key)
	    throws MalformedURLException {

	loadResourceBundle(fileName, locale);
	return resourceBundle.getString(key);

    }
}
