package com.notes.utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateSession {
    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public Session getSession() {

	try {
	    Configuration configuration = new Configuration().configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(
		    configuration.getProperties()).buildServiceRegistry();
	    factory = configuration.buildSessionFactory(serviceRegistry);

	} catch (Throwable ex) {
	    ex.printStackTrace();
	    System.err.println("Failed to create sessionFactory object." + ex);
	    throw new ExceptionInInitializerError(ex);
	}

	Session session = factory.openSession();
	return session;
    }

    public void sessionClose(Session session) {
	session.close();
    }
}
