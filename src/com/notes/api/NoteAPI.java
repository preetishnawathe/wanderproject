package com.notes.api;

import java.util.List;
import java.util.Map;

import com.notes.UIObj.NoteUIObj;
import com.notes.model.Note;

public interface NoteAPI {

    public boolean isAddedNote(String noteTitle);

    public String registerNewNote(String noteTitle, String noteDescription,
	    Long userId);

    public List<NoteUIObj> loadNotes(Long userId);

    public boolean deleteSelectedNote(Long noteId);

    public Map<String, String> getNoteDetails(Long noteId);

    public boolean updateNoteDetails(Long noteId, String noteTitle,
	    String noteDescription);

    public List<Note> getNoteByTitle(String noteTitle);

}
