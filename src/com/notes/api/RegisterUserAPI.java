package com.notes.api;

import java.util.List;

public interface RegisterUserAPI {

    /**
     * 
     * 
     * @param userName
     * @return This Method is used for verifying whether user is present or not
     */

    public String isUserRegistered(String userName);

    public String registerUser(List<String> userData);

}
