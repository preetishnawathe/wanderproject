package com.notes.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.notes.impl.LoginUserAPIImpl;
import com.notes.model.Certificate;

public class LoginNavigateController extends AbstractController {
    Map<String, String> loginList = null;
    private String message;

    public Map<String, String> getLoginList() {
	return loginList;
    }

    public void setLoginList(Map<String, String> loginList) {
	this.loginList = loginList;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	String userStatus = null;
	List userDataList = null;

	HttpSession session = request.getSession();

	Certificate certificate = new Certificate();
	Map<String, String> userDetailsMap = new LinkedHashMap<String, String>();
	String userName = request.getParameter("formusername") == null ? null
		: request.getParameter("formusername");
	String userPassword = request.getParameter("formpassword") == null ? null
		: request.getParameter("formpassword");
	boolean isNavigation = new Boolean(request.getParameter("isNavigation"));
	certificate = session.getAttribute("certificate") == null ? null
		: (Certificate) session.getAttribute("certificate");
	// userPassword=Encryption.Encrypt(userPassword);
	LoginUserAPIImpl loginUserImpl = new LoginUserAPIImpl();
	if (!isNavigation) {
	    userDataList = loginUserImpl.getUserData(userName);
	}
	if (userDataList != null && userDataList.size() > 0 && !isNavigation) {
	    certificate = loginUserImpl.getCertificate(userDataList);
	    session.setAttribute("certificate", certificate);

	    return new ModelAndView("noteMenu.jsp", "session", session);

	} else if (isNavigation) {
	    session.setAttribute("certificate", certificate);

	    return new ModelAndView("noteMenu.jsp", "session", session);

	}
	return null;
    }

    public void setMessage(String message) {
	this.message = message;
    }
}
