package com.notes.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.notes.UIObj.NoteUIListObj;
import com.notes.UIObj.NoteUIObj;
import com.notes.api.NoteAPI;
import com.notes.constants.NoteConstants;
import com.notes.impl.NoteAPIImpl;
import com.notes.model.Certificate;

public class SelectExistingNoteController extends AbstractController {

    private String message;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	NoteUIListObj noteUIListObj = new NoteUIListObj();
	Long userId = request.getParameter(NoteConstants.REQUEST_USER_ID) == null ? 0L
		: new Long(request.getParameter(NoteConstants.REQUEST_USER_ID));
	List<NoteUIObj> noteList = new ArrayList<NoteUIObj>();
	NoteAPI noteApi = new NoteAPIImpl();
	noteList = noteApi.loadNotes(userId);
	noteUIListObj.setNoteUIObj(noteList);
	noteUIListObj.setCerticate((Certificate) request.getSession()
		.getAttribute("certificate"));

	return new ModelAndView("selectExistingNote.jsp", "noteUIObj",
		noteUIListObj);
    }

    public void setMessage(String message) {
	this.message = message;
    }

}
