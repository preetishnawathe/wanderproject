package com.notes.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class HomePageController extends AbstractController {

    private String message;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	boolean isLogout = new Boolean(request.getParameter("isLogout"));
	if (isLogout) {
	    HttpSession userSession = request.getSession();
	    userSession.invalidate();
	}

	return new ModelAndView("login.jsp", "welcomeMessage", message);
    }

    public void setMessage(String message) {
	this.message = message;
    }

}
