package com.notes.constants;

public class NoteConstants {

    public static final String DATE_FORMAT = "dd/MM/yy hh:mm:ss a";

    public static final String REQUEST_USER_ID = "REQUEST_USER_ID";

}
