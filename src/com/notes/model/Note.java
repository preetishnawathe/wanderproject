package com.notes.model;

import java.io.Serializable;
import java.util.Date;

public class Note implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long noteId;
    private String noteTitle;
    private String noteDescription;
    private Date noteCreationDate;
    private Date noteModificationDate;
    private Long noteCreatorId;

    public Long getNoteId() {
	return noteId;
    }

    public void setNoteId(Long noteId) {
	this.noteId = noteId;
    }

    public String getNoteTitle() {
	return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
	this.noteTitle = noteTitle;
    }

    public String getNoteDescription() {
	return noteDescription;
    }

    public void setNoteDescription(String noteDescription) {
	this.noteDescription = noteDescription;
    }

    public Date getNoteCreationDate() {
	return noteCreationDate;
    }

    public void setNoteCreationDate(Date noteCreationDate) {
	this.noteCreationDate = noteCreationDate;
    }

    public Date getNoteModificationDate() {
	return noteModificationDate;
    }

    public void setNoteModificationDate(Date noteModificationDate) {
	this.noteModificationDate = noteModificationDate;
    }

    public Long getNoteCreatorId() {
	return noteCreatorId;
    }

    public void setNoteCreatorId(Long noteCreatorId) {
	this.noteCreatorId = noteCreatorId;
    }

}
