package com.notes.model;

public class Certificate {
    private String userFirstName;
    private String userLastName;
    private String userName;
    private String userPassword;
    private Long userId;

    public String getUserPassword() {
	return userPassword;
    }

    public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
    }

    public String getUserFirstName() {
	return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
	this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
	return userLastName;
    }

    public void setUserLastName(String userLastName) {
	this.userLastName = userLastName;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

}
