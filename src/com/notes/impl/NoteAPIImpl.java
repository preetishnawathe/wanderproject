package com.notes.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.notes.UIObj.NoteUIObj;
import com.notes.api.NoteAPI;
import com.notes.constants.NoteConstants;
import com.notes.dao.api.NoteDAO;
import com.notes.dao.impl.NoteDAOImpl;
import com.notes.model.Note;
import com.notes.utility.DataHandler;

public class NoteAPIImpl implements NoteAPI {
    DataHandler dataHandler = new DataHandler();
    NoteDAO noteDao = new NoteDAOImpl();
    static Logger logger = Logger.getLogger(NoteAPIImpl.class);

    public boolean isAddedNote(String noteTitle) {

	return noteDao.isAddedNote(noteTitle);
    }

    public String registerNewNote(String noteTitle, String noteDescription,
	    Long userId) {
	String message = null;
	Note note = new Note();
	try {
	    note.setNoteTitle(noteTitle);
	    note.setNoteDescription(noteDescription);
	    note.setNoteCreationDate(new Date());
	    note.setNoteCreatorId(userId);
	    message = "NOTE_ADDED";
	    noteDao.addNote(note);
	    logger.info("Note" + noteTitle + "has been added to system");
	} catch (Exception e) {
	    logger.error("Error in adding new Note", e);
	    message = "NOTE_FAILED";
	}
	return message;
    }

    public List<NoteUIObj> loadNotes(Long userId) {
	List<Note> noteList = new ArrayList<Note>();
	List<NoteUIObj> noteObjUIList = new ArrayList<NoteUIObj>();
	try {
	    noteList = noteDao.getNoteDetails(userId);
	    noteObjUIList = populateNoteUIObjs(noteList);
	} catch (Exception e) {
	    logger.error("Error in getting notes data", e);
	}
	return noteObjUIList;
    }

    public boolean deleteSelectedNote(Long noteId) {
	return noteDao.deleteNote(noteId);
    }

    private List<NoteUIObj> populateNoteUIObjs(List<Note> noteList) {
	List<NoteUIObj> noteUIObjList = new ArrayList<NoteUIObj>();
	for (Note note : noteList) {
	    noteUIObjList.add(populateNoteUIObj(note));
	}
	return noteUIObjList;
    }

    private NoteUIObj populateNoteUIObj(Note note) {
	NoteUIObj noteUIObj = new NoteUIObj();

	noteUIObj.setNoteTitle(note.getNoteTitle());
	noteUIObj.setNoteDescription(note.getNoteDescription());
	noteUIObj.setCreationDate(note.getNoteCreationDate());
	noteUIObj.setModificationDate(note.getNoteModificationDate());
	noteUIObj.setUserId(note.getNoteCreatorId());
	noteUIObj.setNoteId(note.getNoteId());
	return noteUIObj;
    }

    public Map<String, String> getNoteDetails(Long noteId) {
	Note noteDbObject = noteDao.getNote(noteId);
	Map<String, String> noteDetailsMap = new LinkedHashMap<String, String>();

	noteDetailsMap.put("noteTitle", noteDbObject.getNoteTitle());
	noteDetailsMap
		.put("noteDescription", noteDbObject.getNoteDescription());
	noteDetailsMap.put("noteCreationDate", new SimpleDateFormat(
		NoteConstants.DATE_FORMAT).format(noteDbObject
		.getNoteCreationDate()));
	noteDetailsMap.put("noteModificationDate", noteDbObject
		.getNoteModificationDate() != null ? new SimpleDateFormat(
		NoteConstants.DATE_FORMAT).format(noteDbObject
		.getNoteModificationDate()) : "N/A");
	return noteDetailsMap;

    }

    public boolean updateNoteDetails(Long noteId, String noteTitle,
	    String noteDescription) {
	Session session = dataHandler.getSession();
	boolean isSucessful = true;
	try {
	    Note noteDbObject = noteDao.getNote(noteId);
	    noteDbObject.setNoteTitle(noteTitle);
	    noteDbObject.setNoteDescription(noteDescription);
	    noteDbObject.setNoteModificationDate(new Date());
	    dataHandler.updateObj(session, noteDbObject);
	} catch (Exception e) {
	    logger.error("Error in update of note", e);
	    dataHandler.transactionRollback(session);
	    isSucessful = false;
	} finally {
	    dataHandler.sessionClose(session);
	}
	return isSucessful;
    }

    public List<Note> getNoteByTitle(String noteTitle) {
	return noteDao.getNoteByTitle(noteTitle);
    }
}
