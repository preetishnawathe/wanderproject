package com.notes.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.notes.api.RegisterUserAPI;
import com.notes.dao.api.RegisterDAO;
import com.notes.dao.impl.RegisterDAOImpl;
import com.notes.model.User;
import com.notes.utility.DataHandler;
import com.notes.utility.Encryption;

public class RegisterUserAPIImpl implements RegisterUserAPI {
    DataHandler dataHandler = new DataHandler();
    RegisterDAO registerDAOAPI = new RegisterDAOImpl();
    static Logger logger = Logger.getLogger("RegisterUserAPIImpl");

    public String isUserRegistered(String userName) {

	return registerDAOAPI.isUserExists(userName);

    }

    public String registerUser(List<String> userData) {
	Session session = null;
	String message = null;
	session = dataHandler.getSession();
	try {
	    String userName = userData.get(0);
	    String userPassword = userData.get(1);
	    String userFirstName = userData.get(2);
	    String userLastName = userData.get(3);
	    String encryptedPassword = Encryption.encrypt(userPassword);

	    User userDetails = new User();
	    userDetails.setUserFirstName(userFirstName);
	    userDetails.setUserLastName(userLastName);
	    userDetails.setUserPassword(encryptedPassword);
	    userDetails.setUserName(userName);
	    dataHandler.saveObj(session, userDetails);

	    message = "REGISTRATION_SUCCESSFUL";
	    logger.info("User" + userName + "is added to system. User ID : "
		    + userDetails.getUserId());

	} catch (Exception e) {
	    logger.error("Error in registering user", e);
	    message = "REGISTRATION_FAILED";
	    dataHandler.transactionRollback(session);
	} finally {
	    dataHandler.sessionClose(session);
	}
	return message;
    }

}
