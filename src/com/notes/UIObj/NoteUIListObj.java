package com.notes.UIObj;

import java.util.ArrayList;
import java.util.List;

import com.notes.model.Certificate;

public class NoteUIListObj {

    List<NoteUIObj> NoteUIObj = new ArrayList<NoteUIObj>();

    Certificate certificate;

    public List<NoteUIObj> getNoteUIObj() {
	return NoteUIObj;
    }

    public void setNoteUIObj(List<NoteUIObj> noteUIObj) {
	NoteUIObj = noteUIObj;
    }

    public Certificate getCerticate() {
	return certificate;
    }

    public void setCerticate(Certificate certificate) {
	this.certificate = certificate;
    }

}
