package com.notes.UIObj;

import java.util.Date;

import com.notes.model.Certificate;

public class NoteUIObj {

    private Certificate certificate;

    private String noteTitle;

    private String noteDescription;

    private Date creationDate;

    private Date modificationDate;

    private Long userId;

    private Long noteId;

    public Certificate getCertificate() {
	return certificate;
    }

    public void setCertificate(Certificate certificate) {
	this.certificate = certificate;
    }

    public String getNoteTitle() {
	return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
	this.noteTitle = noteTitle;
    }

    public String getNoteDescription() {
	return noteDescription;
    }

    public void setNoteDescription(String noteDescription) {
	this.noteDescription = noteDescription;
    }

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    public Date getModificationDate() {
	return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
	this.modificationDate = modificationDate;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public Long getNoteId() {
	return noteId;
    }

    public void setNoteId(Long noteId) {
	this.noteId = noteId;
    }

}
