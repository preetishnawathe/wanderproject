package com.notes.dwr;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.notes.impl.LoginUserAPIImpl;
import com.notes.impl.RegisterUserAPIImpl;

public class DwrLoginController {
    LoginUserAPIImpl loginUserAPIImpl = new LoginUserAPIImpl();

    public String registerUser(String userName, String userPassword,
	    String userFirstName, String userLastName,
	    HttpServletRequest request, HttpServletResponse response) {
	String userStatus = null;
	List<String> userList = new ArrayList<String>();
	RegisterUserAPIImpl rs = new RegisterUserAPIImpl();
	userStatus = rs.isUserRegistered(userName);
	userList.add(userName);
	userList.add(userPassword);
	userList.add(userFirstName);
	userList.add(userLastName);
	if ("USER_NOT_PRESENT".equalsIgnoreCase(userStatus)) {
	    userStatus = rs.registerUser(userList);

	}
	return userStatus;
    }

    public boolean isUserExists(String userName, String userPassword,
	    HttpServletRequest request, HttpServletResponse response) {
	boolean isUserExists = false;
	isUserExists = loginUserAPIImpl.isUserExists(userName, userPassword);
	return isUserExists;
    }

}
