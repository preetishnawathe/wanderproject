package com.notes.dwr;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.notes.api.NoteAPI;
import com.notes.impl.NoteAPIImpl;

public class DwrNoteController {

    NoteAPI noteAPI = new NoteAPIImpl();

    public String addNote(String noteTitle, String noteDescription,
	    Long userId, HttpServletRequest request,
	    HttpServletResponse response) throws ParseException {
	String userStatus = null;
	boolean isAddedNote = false;

	isAddedNote = noteAPI.isAddedNote(noteTitle);

	if (!isAddedNote) {
	    userStatus = noteAPI.registerNewNote(noteTitle, noteDescription,
		    userId);
	} else {
	    userStatus = "NOTE_PRESENT";
	}
	return userStatus;
    }

    public boolean deleteNote(Long noteId, HttpServletRequest request,
	    HttpServletResponse response) {
	return noteAPI.deleteSelectedNote(noteId);
    }

    public Map<String, String> getNoteDetails(Long noteId,
	    HttpServletRequest request, HttpServletResponse response) {
	return noteAPI.getNoteDetails(noteId);
    }

    public boolean updateNoteDetails(Long noteId, String noteTitle,
	    String noteDescription, HttpServletRequest request,
	    HttpServletResponse response) {

	return noteAPI.updateNoteDetails(noteId, noteTitle, noteDescription);

    }

}
