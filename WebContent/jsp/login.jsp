<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="com.notes.utility.ResourceBundleResources"%>
<%@page import="java.util.Locale"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOGIN_PAGE_TITLE")%></title>
<link href="<%=request.getContextPath() %>/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/css/application.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/jquery.alerts.css" media="screen"/>
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/jquery.ui.all.css"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/popup.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.alerts.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/validate.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.blockUI.js"></script>
 <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/engine.js"></script>
<script type='text/javascript' src="<%=request.getContextPath() %>/dwr/util.js"></script>
    <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/interface/DwrLoginController.js"></script>	
   <script>
   function loginAndValidateUser()
   {
	   var loginFlag=validateUserLogin();
	   if(loginFlag){
		   loginUser();
	  }  
   }
   
   function loginUser(){
	   
	   var loginUserName=jQuery("#loginusername").val();
	   var loginPassword=jQuery("#loginpassword").val();
	   $('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOGGING_IN_MESSAGE")%></h3>',centerY:0});
	   DwrLoginController.isUserExists(loginUserName,loginPassword,function(isUserExists){
			if(isUserExists){
				   var loginform=document.getElementById("loginform");
				   loginform.action ="<%=request.getContextPath()+"/notes.dsx"%>";
				   jQuery('#formusername').val(loginUserName);
				   jQuery('#formpassword').val(loginPassword);
				   loginform.submit();
			}else{
				$('body').unblock();
				jQuery("#errorMessage").text("<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOGIN_ERROR_MESSAGE")%>");
				jQuery("#errorLoginLabel").attr("style","display:block");
				jQuery("#loginusername").addClass('dull');
				jQuery("#loginusername").val('');
				jQuery("#loginusername").blur();
				jQuery("#loginpassword").hide();
				jQuery('#fakepassword').attr('value',"<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"PASSWORD_TITLE")%>");
	            jQuery('#fakepassword').addClass('dull');
	            jQuery('#fakepassword').show();
				
			}		
	   }); 
	   
	   
   }
   function registerUser() {
	   var userName=jQuery("#username").val();
	   var password=jQuery("#userpassword").val();
	   var firstName=jQuery("#userfirstname").val();
	   var lastName=jQuery("#userlastname").val();
	   
	   jQuery('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3 class=""><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"REGISTRATION_MESSAGE")%></h3>',centerY:0});
	   DwrLoginController.registerUser(userName,password,firstName,lastName,function(userStatus){
		   HideDialog('dialogRegister');
		   $('body').unblock();
		   if(userStatus=="USER_PRESENT"){
	        	jAlert('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"REGISTRATION_USER_PRESENT")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ALERT")%>');
	        }
	        else if(userStatus=="REGISTRATION_FAILED"){
	        	jError('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"REGISTRATION_USER_FAILED")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ERROR")%>');
	        }
	        else if(userStatus="REGISTRATION_SUCCESSFUL"){
	        	jSuccess('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"REGISTRATION_SUCCESSFUL")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_SUCCESSFUL")%>');
	        }
		   

		   });

	

	}

   function resetRegisterUser(){
  		jQuery("#username").val("");
  		jQuery("#userpassword").val("");
  		jQuery("#userfirstname").val("");
  		jQuery("#userlastname").val("");
  		jQuery("#usercpassword").val("");
  		jQuery('#username').focus();
  		jQuery('#userpassword').focus();
  		jQuery('#userfirstname').focus();
  		jQuery('#userlastname').focus();
  		jQuery('#usercpassword').focus();
  	}
   	function validateLogin(event){
		if (event.keyCode == 13) {
			loginAndValidateUser();
	        return false;
	    }
		
	}

	
	   function pwdFocus(fakePassword,password) {
           $('#'+fakePassword).hide();
           $('#'+password).attr("value","");
           $('#'+password).show();
           $('#'+password).focus();
       }

       function pwdBlur(fakePassword,password,text) {
           if ($('#'+password).attr('value') == '') {
               $('#'+password).hide();
               $('#'+fakePassword).attr("value",text);
               $('#'+fakePassword).addClass('dull');
               $('#'+fakePassword).show();
           }
       }


  </script> 
</head>
<body id="home">
<div id="templatemo_wrapper">
	<div id="templatmeo_header">
    	

    </div> <!-- end of header -->
    
    <div id="templatemo_middle">
    	
        <div id="mid_left">
            <div id="mid_title"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTES_WEB_APP_LABEL")%></div>
            <p><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTES_WEB_APP_LABEL1")%></p>
            
	  </div>
        <div class="cleaner"></div>
	</div> <!-- end of middle -->
    
     <div id="templatemo_main">
        <div id="templatemo_content">
        
        	
	<form action="LoginNavigateController" name="login" id="loginform" method="post">
	<input type="hidden" id="formusername" name="formusername"  value="">
	<input type="hidden" id="formpassword" name="formpassword" value="">
	<input type="hidden" id="isNavigation" name="isNavigation" value="false">
	</form>
	<br/>
	
<div class="ui-widget" id="errorLoginLabel"  style="display:none;width:350px">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;width:350px">
		<p ><span class="ui-icon ui-icon-alert" style="float: left; margin-right: 2px;margin-top:4px"></span>
		<label style="float: left; margin-right: 2px;margin-top:4px" id="errorMessage"></label></p>
	</div>
	</div>
		<table>
<tr>
	<td  width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"USERNAME_LABEL")%></label></td>
	<td><input type="text" value="" id="loginusername" name="username" class="roundedrectangle" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"USERNAME_TITLE")%>"/><td><br><br>
</tr>
<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"PASSWORD_LABEL")%></label></td>
	<td><input class="roundedrectangle" type="text" name="fakepassword" id="fakepassword" title ="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"PASSWORD_TITLE")%>" value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"PASSWORD_TITLE")%>" onfocus="pwdFocus('fakepassword','loginpassword')" />
    <input class="roundedrectangle" style="display: none" type="password" name="password" id="loginpassword" value="" onblur="pwdBlur('fakepassword','loginpassword','<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"PASSWORD_TITLE")%>')" onkeypress="validateLogin(event)" /><td><br><br>
</tr>

<tr> <td><div class="roundedbutton">
	
<input type="button"  value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOG_IN")%>" class="roundedbutton current" onclick="loginAndValidateUser()" >
</div></td>
</tr>

</table><br/>
<label class="textlabel" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SIGN_UP_MESSAGE_1")%> <a href="javascript:ShowDialog(true,'dialogRegister');"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SIGN_UP_MESSAGE_2")%></a> <%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SIGN_UP_MESSAGE_3")%></label>
			
            
            <div class="col_allw300 col_last">
            	
            </div>
            <div class="cleaner"></div>
        </div> <!-- end of templatemo_content -->
    </div> <!-- end of templatemo_main -->
</div> <!-- end of wrapper -->

<div id="templatemo_footer_wrapper">
 
     <div class="cleaner"></div>
</div>


<div id="templatemo_copyright_wrapper">
    <div id="templatemo_copyright">
    	
            <label><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"COPYRIGHT")%> </label>
        
    </div> <!-- end of templatemo_footer -->
</div>





	<div id="overlay" class="web_dialog_overlay"></div>
    
    <div id="dialogRegister" class="web_dialog " style="display:none">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr>
                <td class="web_dialog_title" width="40%"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"REGISTER_LABEL")%></td>
                <td class="web_dialog_title align_right">
                <label class="close right" style="display:block; cursor:pointer" onclick="HideDialog('dialogRegister')" ></label>
                   
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           <tr>
	<td  width="30%"><label class="textlabelpopup"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"USERNAME_LABEL")%></label></td>
	<td width="70%"><input type="text" value="" id="username" class="roundedrectangle" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_USER_NAME")%>"/></td>
	</tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           <tr>
	<td  width="30%"><label class="textlabelpopup"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"FIRST_NAME_LABEL")%></label></td>
	<td width="70%"><input type="text" value="" id="userfirstname" class="roundedrectangle" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_FIRST_NAME")%>"/></td>
	</tr>
	 <tr>
         <td>&nbsp;</td>
      </tr>
<tr>
	<td width="30%"><label class="textlabelpopup"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LAST_NAME_LABEL")%></label></td>
	<td width="70%"><input type="text" value="" id="userlastname" class="roundedrectangle"  title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_LAST_NAME")%>"/></td>
</tr>
      <td>&nbsp;</td>
      </tr>
<tr>
	<td width="30%"><label class="textlabelpopup"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NEW_PASSWORD_LABEL")%></label></td>
	<td width="70%">
	<input class="roundedrectangle" type="text" name="fakepassword" id="userfakepassword" title ="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_NEW_PASSWORD")%>" value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_NEW_PASSWORD")%>" onfocus="pwdFocus('userfakepassword','userpassword')" />
    <input class="roundedrectangle" style="display: none" type="password" name="password" id="userpassword" value="" onblur="pwdBlur('userfakepassword','userpassword','<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ENTER_NEW_PASSWORD")%>')" /></td>
</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>
   <tr>
	<td width="30%"><label class="textlabelpopup"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"RE_ENTER_PASSWORD_LABEL")%></label></td>
	<td width="70%">
	<input class="roundedrectangle" type="text" name="fakepassword" id="usercfakepassword" title ="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"RE_ENTER_PASSWORD_LABEL")%>" value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"RE_ENTER_PASSWORD_LABEL")%>" onfocus="pwdFocus('usercfakepassword','usercpassword')" />
    <input class="roundedrectangle" style="display: none" type="password" name="password" id="usercpassword" value="" onblur="pwdBlur('usercfakepassword','usercpassword','Re-Enter New Password')" /></td>
</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>
<tr>
         <td>&nbsp;</td>
      </tr>
<tr> <td><div class="roundedbutton">
	<input type="button"  value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SAVE_DETAILS")%>" class="roundedbutton current" onclick="registerUser()" >
</div></td>
<td><div class="roundedbutton">
	<input type="button"  value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"RESET")%>" class="roundedbutton" onclick="resetRegisterUser();" >

</div></td>

</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>

        </table>
    </div>
    
    
    

</body><script>
$('input[type="text"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});




</script>
</html>