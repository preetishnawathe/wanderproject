<%@page import="java.util.List"%>

<%@page import="com.notes.model.Certificate"%>


<%@page import="java.util.Locale"%>
<%@page import="com.notes.utility.ResourceBundleResources"%>
<%@page import="com.notes.UIObj.NoteUIObj"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.notes.UIObj.NoteUIListObj"%><html>
<head>
 <link rel="Stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/wwselect.css" />
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/jquery.ui.all.css">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.1.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/wwselect.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/validate.js"></script>
 <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/engine.js"></script>
<script type='text/javascript' src="<%=request.getContextPath() %>/dwr/util.js"></script>
    <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/interface/DwrNoteController.js"></script>	
    <%NoteUIListObj noteUiListObj=(NoteUIListObj)request.getAttribute("noteUIObj"); 
    List<NoteUIObj> noteUIObjList = noteUiListObj.getNoteUIObj();
    Certificate certificate=noteUiListObj.getCerticate();
    Long userId=certificate.getUserId();
    SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
  	
    %>
<script>
var selectedReportArray=new Array();
var isSelectAllChecked = false;
var noteId;
function viewNoteDetails(id,dialog){
	noteId=id;
	$('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOADING_LABEL")%></h3>',centerY:0});
	DwrNoteController.getNoteDetails(id,function(dataMap){
		if(dataMap){
			if(dialog=='dialogShowNote'){
				jQuery('#noteViewTitle').text(dataMap.noteTitle);
				jQuery('#noteViewDesc').text(dataMap.noteDescription);
				jQuery('#noteViewCDate').text(dataMap.noteCreationDate);
				jQuery('#noteViewMDate').text(dataMap.noteModificationDate);
			}else if(dialog='dialogEditNote'){
				jQuery('#noteEditTitle').val(dataMap.noteTitle);
				jQuery('#noteEditDesc').val(dataMap.noteDescription);
				jQuery('#noteEditCDate').val(dataMap.noteCreationDate);
				jQuery('#noteEditMDate').val(dataMap.noteModificationDate);
				jQuery('#noteSave').attr("onclick",'updateDetails('+id+')');
			}
		$('body').unblock();
		 ShowDialog(true,dialog);
		}else{
			$('body').unblock();
			jError('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SERVER_ERROR_LABEL")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ERROR")%>');
		}});
}


   function deleteNote(id){
	   jConfirm('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"CONFIRMATION_DIALOG_DELETE_NOTE")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"CONFIRMATION_LABEL")%>', function(response) {
		   if(response){
	   $('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOADING_LABEL")%></h3>',centerY:0});
	   DwrNoteController.deleteNote(id,function(userStatus){
			if(userStatus){
				jSuccess('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"DELETE_NOTE_SUCCESSFUL")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_SUCCESSFUL")%>');
	        }
			else{
				jError('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"DELETE_NOTE_FAILURE")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ERROR")%>');
	        }
	        
			$('body').unblock();
			showAddUserMenu('select',<%=userId%>);
		}
		           
		    );
		   }
   });
   }

   function noteSearch(isSearch,isLoader){
	   var keyValue;
	   	if(isSearch){
	   		if(isLoader){
	   			$('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SEARCH_LABEL")%></h3>',centerY:0});
	   		}
	   		keyValue = jQuery("#searchTextBox").val();
	   	}else{
	           if(isLoader){
	   			$('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"CLEAR_SEARCH_LABEL")%></h3>',centerY:0});
	           }
	   		keyValue = "";
	   		jQuery("#searchTextBox").val("");
	   	}		
	   	
	       var hiddenRows = 0;
	       var totalRows=0;
	       jQuery("#noResultTr").hide();
	   	var rows=jQuery("#noteTable").find("tbody > tr:gt(0)");
	       totalRows = rows.length;
	   	for(var rowCounter = 0;rowCounter < rows.length;rowCounter++){
	   		var tr = rows[rowCounter];
	   		var id = jQuery(tr).attr("id");
	   	
	   		var rowValue = jQuery("#"+id).find("td:first").text();
	   		
	   			if (rowValue.toLowerCase().indexOf(keyValue.toLowerCase()) < 0) {
	   				jQuery("#"+id).hide();
	   				hiddenRows++;
	   			}
	               else {
	               	jQuery("#"+id).show();
	               }
	   			if(hiddenRows == totalRows){
	   				jQuery("#noResultTr").show();				
	   			}else{
	   				jQuery("#noResultTr").hide();
	   			}	
	   			
	   		}
	   	$('body').unblock();
	   }
function updateDetails(id){

		var noteTitle=jQuery("#noteEditTitle").val();
		var noteDescription=jQuery("#noteEditDesc").val();
	 
	   $('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOADING_LABEL")%></h3>',centerY:0});
	   DwrNoteController.updateNoteDetails(id,noteTitle,noteDescription,function(userStatus){
			HideDialog('dialogEditNote');
			$('body').unblock();
			if(userStatus){
				jSuccess('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"UPDATE_NOTE")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_SUCCESSFUL")%>');
	        }
			else{
				jError('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"UPDATE_NOTE_FAILURE")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ERROR")%>');
	        }
			showAddUserMenu('select',<%=userId%>);
		}           
		    );
}

   
    </script>
</head>
<body>
	<div class="roundedpages" style="width:1147px">
	
	<div>
	 		<input type="text" id="searchTextBox" onkeyup="noteSearch(true,false)" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SEARCH_TITLE")%>" align="left" class="roundedrectangle" style="height:25px;box-shadow:0px 0px 0px #666666"/>
	 			<label onclick="noteSearch(true,true)" class="cursor textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"SEARCH_TEXT_TITLE")%></label>
	 			<label>&nbsp;|&nbsp;</label>
	 			<label onclick="noteSearch(false,true)" class="cursor textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"CLEAR_SEARCH_TITLE")%></label>
			<br/>
	 		<br/>

			<table class="tableContentClass"  cellspacing="10">
	 		<thead class="outerTheadTr">
	 		
    		<tr class="outerTheadTr">
       				<th width="17%" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE")%></th>
       				<th width="25%" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_CREATION_DATE")%></th>
       				<th width="20%" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_MODIFICATION_DATE")%></th>
      				<th ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_ACTION")%></th>
    				</tr>
    				
    		
  				</thead>
   			</table>
	</div>
	<div class="wizardOverflow">

				<table id="noteTable" class="tableContentClass" cellspacing="10">
	 			<tbody>
	 		<%
	 		String display=null;
	 		if(noteUIObjList.size()==0){
				display="display:block";
	 		}else{
	 		   display="display:none";
	 		}
	 		%>
	 		<tr class="alt" id="noResultTr" style="<%=display %>">
				<td><label><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NO_RESULTS_FOUND")%></label></td>
				<td></td>
				</tr>
<%
        	String rowClass="";
        	for(int counter=0;counter<noteUIObjList.size();counter++){
        	    NoteUIObj noteUIObj = noteUIObjList.get(counter);
        	    Long noteId= noteUIObj.getNoteId();
			if(counter%2==0){
				rowClass="alt";
			}
			else{
				rowClass="row";
			}%>
		<tr class="<%=rowClass%>" id="<%=noteId%>">
		<td width="14%"><%=noteUIObj.getNoteTitle() %></td>
		<td width="28%">&nbsp;&nbsp;&nbsp;<%=dateFormat.format(noteUIObj.getCreationDate()) %></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;<%=noteUIObj.getModificationDate()!=null? dateFormat.format(noteUIObj.getModificationDate()) : ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NA_LABEL")%></td>
		<td><label title="View Note" style="cursor:pointer" onclick="viewNoteDetails('<%=noteId%>','dialogShowNote')"><img src="<%=request.getContextPath()%>/css/img/view_text.png"> <%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"VIEW_NOTE") %></label>
		<label title="Edit Note" style="cursor:pointer" onclick="viewNoteDetails('<%=noteId%>','dialogEditNote')"><img src="<%=request.getContextPath()%>/css/img/edit.png"> <%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"EDIT_NOTE") %></label>
		<label title="Delete Note" style="cursor:pointer" onclick="deleteNote('<%=noteId%>')"><img src="<%=request.getContextPath()%>/css/img/delete.png"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"DELETE_NOTE") %></label></td>
	</tr>
<%} %>
</tbody>
</table>






</div>
</div>
	<div id="overlay" class="web_dialog_overlay"></div>
    
    <div id="dialogEditNote" class="web_dialog ">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr>
                <td class="web_dialog_title" width="40%"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"EDIT_NOTE_DETAILS")%></td>
                <td class="web_dialog_title align_right">
                <label class="close right" style="display:block; cursor:pointer" onclick="HideDialog('dialogEditNote')" ></label>
                   
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           <tr>
	<td  width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE")%></label></td>
	<td width="70%"><input type="text" value="" id="noteEditTitle" class="roundedrectangle" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE_TITLE")%>"/></td>
	</tr>
	 <tr>
         <td>&nbsp;</td>
      </tr>
      <tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DESCRIPTION")%></label></td>
	<td width="70%"><input type="text" value="" id="noteEditDesc" class="roundedrectangle"  title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DESC_TITLE")%>"/><td>
</tr>
 <tr>
         <td>&nbsp;</td>
 </tr>
<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_CREATION_DATE")%></label></td>
	<td width="70%"><input type="text" value="" id="noteEditCDate" class="roundedrectangle" disabled="disabled"  /></td>
</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>
<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_MODIFICATION_DATE")%></label></td>
	<td width="70%"><input type="text" value="" id="noteEditMDate" class="roundedrectangle" disabled="disabled" /><td>
</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>

<tr> <td><div class="roundedbutton">
	<input type="button"  value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_UPDATE_DETAILS")%>" class="roundedbutton current" id="noteSave" onclick="updateDetails()" >
</div></td>


</tr>
 <tr>
         <td>&nbsp;</td>
      </tr>
<tr>
        </table>
    </div>
 

<div id="dialogShowNote" class="web_dialog ">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr>
                <td class="web_dialog_title" width="40%"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DETAILS")%></td>
                <td class="web_dialog_title align_right">
                <label class="close right" style="display:block; cursor:pointer" onclick="HideDialog('dialogShowNote')" ></label>
                   
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           <tr>
	<td  width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE")%></label></td>
	<td width="70%"><label id="noteViewTitle" class="textview"></label></td>
	</tr>
      <tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DESCRIPTION")%></label></td>
	<td width="70%"><label id="noteViewDesc" class="textview" ></label></td>
</tr>
<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_CREATION_DATE")%></label></td>
	<td width="70%"><label id="noteViewCDate" class="textview" ></label></td>
</tr>
<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_MODIFICATION_DATE")%></label></td>
	<td width="70%"><label id="noteViewMDate" class="textview"></label></td>
</tr>

<tr>
        </table>
    </div>





<script type="text/javascript">
$('input[type="text"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});

$('input[type="password"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});
</script>
</body>

</html>