
<%@page import="com.notes.utility.ResourceBundleResources"%>
<%@page import="java.util.Locale"%>
<HTML>
<HEAD>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/jquery.ui.all.css">

<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.ui.widget.js"></script>
 <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/engine.js"></script>
<script type='text/javascript' src="<%=request.getContextPath() %>/dwr/util.js"></script>
 <script type="text/javascript" src="<%=request.getContextPath() %>/js/validate.js"></script>
    <script type='text/javascript' src="<%=request.getContextPath() %>/dwr/interface/DwrNoteController.js"></script>	
<script>
	
	function validateAndAddNote(){
			var noteTitle=jQuery("#noteTitle").val();
			var noteDescription=jQuery("#noteDescription").val();
			var userId=jQuery("#userId").val();
	var flag=validateAddNote();
	if(flag){
		$('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ADDING_NOTE_MESSAGE")%></h3>',centerY:0});
		  
		DwrNoteController.addNote(noteTitle,noteDescription,userId,function(userStatus){
			if(userStatus=="NOTE_PRESENT"){
				jAlert('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_LABEL")%> '+noteTitle+' <%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_LABEL_1")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ALERT")%>');
	        }
	        else if(userStatus=="NOTE_FAILED"){
	        	jError('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_ADDITION_MESSAGE")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_ERROR")%>');
	        }
	        else if(userStatus="NOTE_ADDED"){
	        	jSuccess('<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_ADDITION")%>', '<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"JQUERY_SUCCESSFUL")%>');
	        }
	        
			$('body').unblock();
			reload();
		}
		        
		    );
		}
	}
	
function reload(){

	var userId=jQuery("#userId").val();

	showAddUserMenu('add',userId);
}

function reset(){
	jQuery("#noteTitle").val("");
	jQuery("#noteDescription").val("");
	jQuery('#noteTitle').focus();
	jQuery('#noteDescription').focus();
	jQuery('#noteDescription').blur();
}
	
	</script>
</HEAD>
<BODY>





	<div class="roundedpages" style="width:1147px">
	<center>
	<table>
<tr>
	<td  width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE")%></label></td>
	<td width="70%"><input type="text" value="" id="noteTitle" class="roundedrectangle" title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_TITLE_TITLE")%>"/><td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>

<tr>
	<td width="30%"><label class="textlabel"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DESCRIPTION")%></label></td>
	<td width="70%"><input type="text" value="" id="noteDescription" class="roundedrectangle"  title="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTE_DESC_TITLE")%>"/><td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr> 
	<td>
		<div class="roundedbutton">
		<input type="button"  value="Save" class="roundedbutton current" onclick="validateAndAddNote()" >
		</div>
	</td>
	<td>
		<div class="roundedbutton">
		<input type="button"  value="<%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"RESET")%>" class="roundedbutton" onclick="reset()">
		</div>
	</td>

</tr>

</table></center></div>


 


	

	





</BODY>
<SCRIPT>
$('input[type="text"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});

$('input[type="password"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});
</SCRIPT>
</HTML>