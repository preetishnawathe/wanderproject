function validateUserLogin(){
	var username=jQuery("#loginusername").val();
	var password=jQuery("#loginpassword").val();
	 var letterNumber = /^[0-9a-zA-Z]+$/; 
	 var flag=true;
	if(username=="Enter Username"||username==""||username==null){
		jQuery("#errorMessage").text('Username is required for login');
		jQuery("#errorLoginLabel").attr("style","display:block");
		flag=false;
		return false;
	}
    if (username.match(letterNumber)) {
    }else{
    	jQuery("#errorMessage").text('Username should be of alpha-numeric text only.');
    	jQuery("#errorLoginLabel").attr("style","display:block");
    	flag=false;
    	return false;
    }
	if(password=="Enter Password"||password==""||password==null){
		jQuery("#errorMessage").text('Password is required for login');
		jQuery("#errorLoginLabel").attr("style","display:block");
		flag=false;
		return false;
	}
	
	return flag;
}


function showAddUserMenu(id,userId){
	$('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3>Loading data Please wait...</h3>',centerY:0});
	if(id=="add"){
		document.getElementById("addNoteTab").className = "menuroundedtab current";
		document.getElementById("editNoteTab").className = "menuroundedtab";
		$('#addNoteleftmenu').load('addNote.dsx');
		
	}
	else if(id=="select"){
		document.getElementById("addNoteTab").className = "menuroundedtab";
		document.getElementById("editNoteTab").className = "menuroundedtab current";
		$('#addNoteleftmenu').load('selectExistingNote.dsx?REQUEST_USER_ID='+userId);
	}
	$('body').unblock();
}
	
function validateAddNote(){
	var noteTitle=jQuery("#noteTitle").val();
	var noteDescription=jQuery("#noteDescription").val();
	var flag=true;
	var letterNumber = /^[0-9a-zA-Z]+$/; 
	if(noteTitle=="Enter Note's Title"||noteTitle==""||noteTitle==null){
				jAlert('Please enter note title', 'Alert');
		flag=false;
		return;
	}
    if (noteTitle.match(letterNumber)) {
    	
    }
    else{
    	jAlert('Please enter note title with alpha-numeric text only.', 'Alert');
    	flag=false;
    	return;
    }

	if(noteDescription=="Enter Note's Description"||noteDescription==""||noteDescription==null){
		jAlert('Please enter note description.', 'Alert');
		flag=false;
		return;
	}
    if (noteDescription.match(letterNumber)) {
       	
    }
    else{
    	jAlert('Please enter note description with alpha-numeric text only.', 'Alert');
       	flag=false;
    	return;
    }
    
    
    
    
    return flag;
}

function ShowDialog(modal,id)
{
    $("#overlay").show();
    $('#'+id).fadeIn(100);

    if (modal)
    {
        $("#overlay").unbind("click");
    }
    else
    {
        $("#overlay").click(function (e)
        {
            HideDialog(id);
        });
    }
}

function HideDialog(id)
{
    $("#overlay").hide();
    $('#'+id).fadeOut(300);
} 
var selectedReport=new Array();
function addRemoveReport(obj){

	if(obj.checked){
		selectedReport.push(obj.title);
	}
	else{
		selectedReport.remove(obj.title);
	}
		
	}
	
