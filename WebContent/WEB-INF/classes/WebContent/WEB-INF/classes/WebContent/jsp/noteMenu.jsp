<HTML>
<HEAD>
<TITLE>Note's Menu</TITLE>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/default.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/jquery.alerts.css" media="screen"/>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.alerts.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/validate.js"></script>
<%
HttpSession httpSession =(HttpSession)request.getAttribute("session");	
Certificate certificate =(Certificate)httpSession.getAttribute("certificate");
Long userId=certificate.getUserId();
String firstName=certificate.getUserFirstName();
String lastName=certificate.getUserLastName();

%>
</HEAD>


<BODY onLoad="showAddUserMenu('add',<%=userId %>)">

<%@include file="header.jsp" %>
<input type="hidden" id="userId" value="<%=userId %>"/>

<div style="position:fixed; width:100%; height:40px;top:90px; " >
<div class="breadcrumb" style="width:100%"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"BREADCRUMB_LABEL")+" "+ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"BREADCRUMB_NOTES_LABEL")%></div>
</div>

<div class="container" style="margin :125px 0px;height:50%;width:100%" >	

<table>
<tr>
<td>
<table>


<tr>

 <td><div class="menuroundedtab">
 <p class="menuroundedtab" onclick="showAddUserMenu('add',<%=userId %>)"id="addNoteTab"><img style="padding:29px 49px 0px;" src="<%=request.getContextPath() %>/css/img/addicon.png"><br><label style="padding:0px 58px 0px;"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"ADD_NOTE_LABEL")%></label></p>
	

</div></td></tr>
<tr>

 <td><div class="menuroundedtab ">
 <p class="menuroundedtab "onclick="showAddUserMenu('select',<%=userId %>)" id="editNoteTab"><img style="padding:29px 49px 0px;" src="<%=request.getContextPath() %>/css/img/edit user.png" align="center"><br><label style="padding:0px 58px 0px;"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"EDIT_NOTE_LABEL")%></label></p> 
	

</div></td></tr>



</table>
</td>
<td>
<div id="addNoteleftmenu" style="width:1136px"></div>
</td>
</tr>



</table>

	



		<div class="clearer"><span></span></div>

	</div>

	<%@include file="footer.jsp" %>



</BODY>
<SCRIPT>






$('input[type="text"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});

$('input[type="password"]').each(function(){

	this.value = $(this).attr('title');
	$(this).addClass('dull');

	$(this).focus(function(){
		if(this.value == $(this).attr('title')) {
			this.value = '';
			$(this).removeClass('dull');
		}
	});

	$(this).blur(function(){
		if(this.value == '') {
			this.value = $(this).attr('title');
			$(this).addClass('dull');
		}
	});
});
</SCRIPT>
</HTML>