<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.notes.model.Certificate"%>
<%@page import="com.notes.impl.LoginUserAPIImpl"%>


<%@page import="java.util.Locale"%>
<%@page import="com.notes.utility.ResourceBundleResources"%>
<%@page import="com.notes.constants.NoteConstants"%><html>
<HEAD>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/header.css" media="screen"/>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/validate.js"></script>


<STYLE>

</STYLE>
</HEAD>

<script>
<%Date d1=new Date();
SimpleDateFormat dateFormat=new SimpleDateFormat(NoteConstants.DATE_FORMAT);
String dateString=dateFormat.format(d1);

%>
function clock()
    {

    setTimeout("clock()", 1000);
    }
	

	
function logoutUser(){
	   $('body').block({message:'<label class="ajaxloader" style="display:block" ></label><h3><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOGGING_OUT")%></h3>',centerY:0});
		  
	   var loginform=document.getElementById("logoutform");
	   loginform.action ="<%=request.getContextPath()+"/login.dsx"%>";
	   loginform.submit();
}	

function reloadMenu(){
	  var loginform=document.getElementById("logoutform");
	   loginform.action ="<%=request.getContextPath()+"/notes.dsx"%>";
	   loginform.submit();
}
</script>
	
<body >



				

<div style="position:fixed; width:100%; height:30px;top:0px; " >
<div style="padding:0 10px; text-align:right" class="topMenuBackground" >
	
	
		<form action="" id="logoutform" method="post">
		<input type="hidden" name="isLogout" value="true"/>
		<input type="hidden" id="isNavigation" name="isNavigation" value="true"/>
		</form>
		
		<span id="username" class="textlabeluser"    ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"WELCOME_NOTE")%> <%=firstName+" "+lastName %></span>
		| <label id="username" class="textlabelheader"  onclick="logoutUser()"><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"LOGOUT_LABEL")%></label>
		</div>
	

<div class="headernavigation" style="padding:0 10px;">
     

                <label onclick="reloadMenu()" class="current" style="cursor:pointer" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"NOTES_LABEL")%></label> 

          </div>
          <div class="datetime textlabeltime " style="width:100%" >
			<label id="date" ><%=ResourceBundleResources.getValue("com.notes.resources.message",Locale.getDefault(),"DATE_TIME_LABEL")+" "+dateString%></label>
		</div>
          
		
	 
</div>



	<div id="overlay" class="web_dialog_overlay"></div>
    

	
	

	
</body>
</html>